/**
 * 888                             888
 * 888                             888
 * 88888b.   8888b.  88888b.d88b.  88888b.   .d88b.  888d888  8888b.
 * 888 "88b     "88b 888 "888 "88b 888 "88b d88""88b 888P"       "88b
 * 888  888 .d888888 888  888  888 888  888 888  888 888     .d888888
 * 888 d88P 888  888 888  888  888 888 d88P Y88..88P 888     888  888
 * 88888P"  "Y888888 888  888  888 88888P"   "Y88P"  888     "Y888888
 *
 * @category    Samples
 * @package     apac-sdk-java
 * @author      Bambora Online APAC
 * @copyright   Bambora (http://bambora.com)
 */
package samples.examples;

import com.bambora.notification.NotificationTranslator;
import com.bambora.notification.NotificationOfCompletionMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * To use the notification of completion service, you must provide Bambora with an URL that will reach your web service.
 * How you implement the web service is up to you, but you must accept POST as the request method. We highly recommend you to use
 * the provided NotificationTranslator, NotificationOfCompletionMessage and NotificationOfCompletionMessageBuilder classes,
 * since we have done the hard work mapping all the possible fields and values, no need to reinvent the wheel again here.
 * Below we have supplied some sample code of how you could implement the web service, by having a Java Servlet serving the
 * ServerURL endpoint.
 */
@WebServlet(name = "BamboraUpdateAPI", urlPatterns = {"/bambora-notification-update"})
public class NotificationOfCompleteServletExample extends HttpServlet {

    private OrderService orderService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NotificationOfCompletionMessage response = NotificationTranslator.translate(req);
        Order order = orderService.findOrder(response.getSessionId());
        if (order != null) {
            // lets update our order record
            order.setResult(response.getResult().toString());
            order.setAmount(response.getAmount());
            orderService.update(order);
        }
    }
    interface OrderService {
        Order findOrder(String sessionId);
        void update(Order order);
    }

    interface Order {
        void setSessionId(String sessionId);
        void setResult(String result);
        void setAmount(String amount);
    }

}

